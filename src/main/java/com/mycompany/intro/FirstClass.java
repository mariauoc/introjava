package com.mycompany.intro;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author mariauoc
 */
public class FirstClass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//        System.out.println("Introduce tu edad:");
//        int edad = Integer.parseInt(br.readLine());
//        if (edad >= 50) {
//            System.out.println("Estás en la mejor edad");
//        } else if (edad >= 30) {
//            if (edad >= 40) {
//                System.out.println("Edad de oro");
//            }
//            System.out.println("Ni chicha ni limoná");
//        } else {
//            System.out.println("Disfruta de la vida");
//        }
//        System.out.println("De qué instrumento quieres partituras?");
//        String instrumento = br.readLine();
//        switch(instrumento.toLowerCase()) {
//            case "clavicordio":
//            case "piano":
//                System.out.println("Tenemos 8888 partituras");
//                break;
//            case "guitarra":
//                System.out.println("Tenemos 324 partituras");
//                break;
//            case "saxofon":
//                System.out.println("Tenemos 56 partituras");
//                break;
//            default:
//                System.out.println("De ese instrumento no tenemos partituras");
//        }
//        // bucle for
//        for(int i=0; i<=10; i++) {
//            System.out.println(i);
//        }
//        // bucle while (while-do)
//        int contador = 0;
//        while (contador <=10) {
//            System.out.println(contador);
//            contador++;
//        }
//        // bucle do while - sobre todo lo usamos en menús
//        int n = 0;
//        do {
//            System.out.println(n);
//            n++;
//        } while (n<=100);
        int[] numeros = new int[10];
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            numeros[i] = random.nextInt(0, 11);
        }
        //System.out.println(numeros); <= no es python
        // bucle para mostrar los datos del array
        for (int i = 0; i < numeros.length; i++) {
            System.out.print(numeros[i] + "\t");
        }
        // imprimo salto de línea
        System.out.println();
        // Trabajaremos con ArrayList <= equivalente a las listas python
        // Solo con clases / objetos (no tipos primitivos)
        ArrayList<String> nombres = new ArrayList<>();
        nombres.add("Pepe");
        nombres.add("Rosalía");
        nombres.add("Jaime");
        nombres.add("Vivaldi");
        System.out.println(nombres.get(1));
        // recorrer arraylist por posición
        for (int i = 0; i < nombres.size(); i++) {
            System.out.println(nombres.get(i));
        }
        mostrarNombres(nombres);
    }

    private static void mostrarNombres(ArrayList<String> noms) {
        // Recorrer un arraylist con foreach
        for (String nom : noms) {
            System.out.println(nom);
        }
    }
    
    private static int sumar(int a, int b) {
        int suma = a + b;
        return suma;
    }
}
